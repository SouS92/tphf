# TP AngularJS ExpressJS
**Souhaile Safouani**

### Installation

Download node_modules
```sh
$ npm install
```

### Lunch 

**Run the Mongo Server

**Create a folder "uploads" 


```sh
$ npm start
```


### Version
1.0.0

### Tech

Work based on some open source projects to work properly:

* [AngularJS] - HTML enhanced for web apps!
* [Twitter Bootstrap] - great UI boilerplate for modern web apps
* [node.js] - evented I/O for the backend
* [Express] - fast node.js network app framework 
* [Gulp] - the streaming build system
* Brackets - brand new awesome text editor
* MongoDB - NOSQL!
