var express  = require('express');
var app      = express();
var port     = process.env.PORT || 3000;
var mongoose = require('mongoose');
var passport = require('passport');
var flash    = require('connect-flash');
var path = require('path');
var formidable = require('formidable');
var fs = require('fs');



var morgan       = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser   = require('body-parser');
var session      = require('express-session');

var configDB = require('./config/database.js');

// configuration ===============================================================
mongoose.connect(configDB.url); // Connection vers la BD à travers mongoose

require('./config/passport')(passport,formidable); // envoyer l'objet passportJS pour pouvoir se connecter => cible : config/passport.js
app.use(express.static(path.join(__dirname, 'public')));
app.use(morgan('dev')); // log pour les requêtes effectués ///auth /auth/facebook etc...
app.use(cookieParser()); // lecture des cookies
app.use(bodyParser()); 

app.set('view engine', 'ejs'); //préciser le format ejs comme le format de lecture des pages


app.use(session({ secret: 'tpforhiddenfounderssouhailesafouani' })); // session secrète
app.use(passport.initialize());
app.use(passport.session());
app.use(flash()); 

// routage // classe routes.js ====================================================
require('./app/routes.js')(app, passport); // load our routes and pass in our app and fully configured passport

// Démarrage ======================================================================
app.listen(port);
console.log('Hidden Founders, here we go!' + port);