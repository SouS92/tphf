// config/passport.js

//Chargement des variables qu'on devra utiliser pour la connexion
//le module PassportJS
var TwitterStrategy  = require('passport-twitter').Strategy;
var FacebookStrategy = require('passport-facebook').Strategy;
var GoogleStrategy = require('passport-google-oauth').OAuth2Strategy;

// la variable dans laquelle on va stocker les données récupérés à partir de la connexion.
var User       = require('../app/models/user');

// charger les clés propres aux applications créés.
var configAuth = require('./auth');

module.exports = function(passport) {

    // Sérialisation de l'entité user
    passport.serializeUser(function(user, done) {
        done(null, user.id);
    });

    passport.deserializeUser(function(id, done) {
        User.findById(id, function(err, user) {
            done(err, user);
        });
    });
    


    // =========================================================================
    //  ============================FACEBOOK====================================
    // =========================================================================
    passport.use(new FacebookStrategy({

        // pull in our app id and secret from our auth.js file
        clientID        : configAuth.facebookAuth.clientID,
        clientSecret    : configAuth.facebookAuth.clientSecret,
        callbackURL     : configAuth.facebookAuth.callbackURL,
        // on précise les champs qu'on veut récupérer
        profileFields   : ['id', 'displayName', 'photos', 'email']
    },

    // la fonction de retour
    // les paramètres : token de la session, le profile et la méthode de retour qui est done
    //done va nous servir d'envoyer l'object qu'on va créer dans les commandes suivantes
    function(token, refreshToken, profile, done) {

        // mode asynchrone
        // la méthode sera invoqué que lorsque tous les données sont récupérés
        process.nextTick(function() {

            //chercher dans la BD s'il existe un utilisateur avec cet id
            User.findOne    ({ 'facebook.id' : profile.id }, function(err, user) {
                //on cas d'erreur, la méthode done sera invoqué avec err comme paramètre
                // le paramètre user c'est le retour de la méthode findOne
                // soit il est != null et donc il est stocké dans la base
                //l'autre cas c'est qu'il n'existe pas dans la BD
                if (err)
                    return done(err);
                
                //1er cas : utilisateur trouvé dans la BD
                if (user) {
                   //methode de retour et comme paramètre l'utilisateur 
                   // le null fait référence pour l'erreur
                   //comme il n'existe pas d'erreur alors on renvoie un null
                    return done(null, user); 
                } else {
                    // s'il n'existe pas dans la BD, on va l'insérer
                    var newUser            = new User();
                   
                    
                    newUser.facebook.id    = profile.id;                    
                    newUser.facebook.token = token; 

                    newUser.facebook.name  = profile.displayName;
                    newUser.facebook.email = profile.emails[0].value;
                    newUser.facebook.picturelink = profile.photos[0].value;
                  
                    
                    //Insertion du nouveau utilisateur dans la BD
                    newUser.save(function(err) {
                        if (err)
                            throw err;

                        return done(null, newUser);
                    });
                }

            });
        });

    }));
    
    
    
    // =========================================================================
    //  ==================================GOOGLE================================
    // =========================================================================
    passport.use(new GoogleStrategy({

        clientID        : configAuth.googleAuth.clientID,
        clientSecret    : configAuth.googleAuth.clientSecret,
        callbackURL     : configAuth.googleAuth.callbackURL,

    },
    function(token, refreshToken, profile, done) {


        process.nextTick(function() {

            User.findOne({ 'google.id' : profile.id }, function(err, user) {
                if (err)
                    return done(err);

                if (user) {

                    return done(null, user);
                } else {
                    

                    var newUser          = new User();

                    newUser.google.id    = profile.id;
                    newUser.google.token = token;
                    newUser.google.name  = profile.displayName;
                    newUser.google.email = profile.emails[0].value;
                    newUser.google.picturelink = profile._json.picture;
                  
                    newUser.save(function(err) {
                        if (err)
                            throw err;
                        return done(null, newUser);
                    });
                }
            });
        });

    }));
    
    // =========================================================================
    //  ===========================TWITTER======================================
    // =========================================================================
    passport.use(new TwitterStrategy({

        consumerKey     : configAuth.twitterAuth.consumerKey,
        consumerSecret  : configAuth.twitterAuth.consumerSecret,
        callbackURL     : configAuth.twitterAuth.callbackURL

    },
    function(token, tokenSecret, profile, done) {

     
        process.nextTick(function() {

            User.findOne({ 'twitter.id' : profile.id }, function(err, user) {


                if (err)
                    return done(err);

                if (user) {
                    return done(null, user); 
                } else {

                    
                    var newUser                 = new User();

                    newUser.twitter.id          = profile.id;
                    newUser.twitter.token       = token;
                    newUser.twitter.username    = profile.username;
                    newUser.twitter.displayName = profile.displayName;
                    newUser.twitter.derniertweet = profile._json.status.text;// + profile.status.created_at + "  :" + profile.status.text;
                    newUser.twitter.picturelink = profile.photos[0].value;  

                    newUser.save(function(err) {
                        if (err)
                            throw err;
                        return done(null, newUser);
                    });
                }
            });

    });
    }));

};