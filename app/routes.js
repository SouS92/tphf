module.exports = function(app, passport,formidable) {
var path = require('path');
var formidable = require('formidable');
var fs = require('fs');
    // =====================================
    // ===========Page d'accueil============
    // =====================================
    app.get('/', function(req, res) {
        res.render('index.ejs'); // load the index.ejs file
    });


     app.get('/indexupload', function(req, res) {
        res.render('indexupload.ejs'); // load the index.ejs file
    });
    
    app.post('/upload', function(req, res){

  // create an incoming form object
  var form = new formidable.IncomingForm();

  // specify that we want to allow the user to upload multiple files in a single request
  form.multiples = true;

  // store all uploads in the /uploads directory
  form.uploadDir ='./uploads';

  // every time a file has been uploaded successfully,
  // rename it to it's orignal name
  form.on('file', function(field, file) {
    fs.rename(file.path, path.join(form.uploadDir, file.name));
  });

  // log any errors that occur
  form.on('error', function(err) {
    console.log('An error has occured: \n' + err);
  });

  // once all the files have been uploaded, send a response to the client
  form.on('end', function() {
    res.end('success');
  });

  // parse the incoming request containing the form data
  form.parse(req);

});
    // =====================================
    //  ===========REDIRECTION==============
    // =====================================
    
    // route for showing the profile page
    app.get('/profileF', isLoggedIn, function(req, res) {
        res.render('profiles/profileFacebook.ejs', {
            user : req.user 
        });
    });
    
        app.get('/profileG', isLoggedIn, function(req, res) {
        res.render('profiles/profileGoogle.ejs', {
            user : req.user 
        });
    });
    
    
        app.get('/profileT', isLoggedIn, function(req, res) {
        res.render('profiles/profileTwitter.ejs', {
            user : req.user 
        });
    });


//Scope == les permissions
    
    // =====================================
    // =======FACEBOOK ROUTAGE =============
    // =====================================
    app.get('/auth/facebook', passport.authenticate('facebook', { scope : ['email' , 'public_profile','user_photos', 'user_friends'] }));

    app.get('/auth/facebook/callback',
        passport.authenticate('facebook', {
            successRedirect : '/profileF',
            failureRedirect : '/'
        }));
    
    // =====================================
    // ======GOOGLE  ROUTAGE================
    // =====================================
 
    app.get('/auth/google', passport.authenticate('google', { scope: ['https://www.googleapis.com/auth/userinfo.profile', 'https://www.googleapis.com/auth/userinfo.email'] }));

    app.get('/auth/google/callback',
            passport.authenticate('google', {
                    successRedirect : '/profileG',
                    failureRedirect : '/'
            })
           , function(req, res) {
     console.log("sou");   
    }
           );
    
    
    
    // =====================================
    // ========TWITTER  ROUTAGE=============
    // =====================================
    app.get('/auth/twitter', passport.authenticate('twitter'));

    app.get('/auth/twitter/callback',
        passport.authenticate('twitter', {
            successRedirect : '/profileT',
            failureRedirect : '/'
        }));


    // =====================================
    // ============ DECONNEXION ============
    // =====================================
    app.get('/logout', function(req, res) {
        req.logout();
        res.redirect('/');
    });
};



//UN MIDDLEWARE pour s'assurer que l'utilisateur est authentifié
function isLoggedIn(req, res, next) {

    if (req.isAuthenticated())
        return next();

    res.redirect('/');
}