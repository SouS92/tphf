//PATH app/models/user.js


var mongoose = require('mongoose');
var bcrypt   = require('bcrypt-nodejs');


var userSchema = mongoose.Schema({

        facebook         : {
        id           : String,
        token        : String,
        email        : String,
        name         : String,
        picturelink  : String
    },
    twitter          : {
        id           : String,
        token        : String,
        displayName  : String,
        username     : String,
        derniertweet : String,
        picturelink  : String
    },
    google           : {
        id           : String,
        token        : String,
        email        : String,
        name         : String,
        picturelink  : String
    }

});

userSchema.methods.generateHash = function(password) {
    return bcrypt.hashSync(password, bcrypt.genSaltSync(8), null);
};

userSchema.methods.validPassword = function(password) {
    return bcrypt.compareSync(password, this.local.password);
};

module.exports = mongoose.model('User', userSchema);